#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
#
# 7ビットASCIIのフォントグリフデータを定義するCソースコードを標準出力に出力する

require './bdf'
require 'pp'

bdf = load_bdffile(ARGV[0])
bdf = bdf.select{|g| g[:code] >= 0x20 && g[:code] <= 0x7e}
blks = find_continuous_blocks(bdf)

raise "must be continuous" if blks.size != 1
raise "must be start == 32" if blks[0][0] != 32
raise "must be end == 126" if blks[0][1] != 126

ptns = {}
bdf.each do |g|
    raise if g[:w] != 8
    raise if g[:h] != 16

    ptns[g[:code]] = rotate_bitmap(g[:ptn], 8)
end

puts "#if defined(__AVR__)"
puts "# include <avr/pgmspace.h>"
puts "#elif !defined(PROGMEM)"
puts "# define PROGMEM /* empty */"
puts "#endif"
puts ""
puts "const unsigned char font8x16[] PROGMEM = {"
(0x20 .. 0x7e).each do |code|
    puts "\t/* character '#{code.chr}', 0x#{code.to_s(16)} */"
    puts "\t" + ptns[code].join(',') + ','
end
puts "};"
