#!/usr/bin/ruby

require 'libusb'
include LIBUSB

def register_keyseq(vendor, product, key_codes)
    usb = LIBUSB::Context.new
    devices = usb.devices(:idVendor => Vendor, :idProduct => Product)
    raise "cannot found any device" if devices.size == 0

    handle = devices.first.open
    begin
	handle.claim_interface(0)
    rescue LIBUSB::ERROR_BUSY
	handle.detach_kernel_driver(0)
	handle.claim_interface(0)
    end

    data = [0x55, 0xaa, key_codes.length & 0xff, key_codes.length >> 8] + key_codes

    (0 ... data.length).each do |i|
	print "\rWriting #{i + 1} of #{data.length}... "
	r = handle.control_transfer(:bmRequestType => ENDPOINT_OUT | REQUEST_TYPE_VENDOR,
				    :bRequest => 1,
				    :wValue => data[i],
				    :wIndex => i)
    end
    print "done\n"

    (0 ... data.length).each do |i|
	print "\rVerifying #{i + 1} of #{data.length}... "
	r = handle.control_transfer(:bmRequestType => ENDPOINT_IN | REQUEST_TYPE_VENDOR,
				    :bRequest => 2,
				    :wValue => 0,
				    :wIndex => i,
				    :dataIn => 1)
	got = r.unpack('C*')[0]

	if got == data[i]
	    print "(#{data[i]}) ok"
	else
	    print "verification failed (eeprom #{got} expected #{data[i]})\n"
	    raise "verification failed"
	end
    end
    print "\nDone\n"
end
